import useAppStore from "@/zustand/store";
import { useRouter } from "next/navigation";
// import { Router } from "next/router";
import Cookies from "universal-cookie";


export const logout = ()=>{
    const cookies = new Cookies(null, { path: '/' });
    // const router = useRouter();
    const updateState = useAppStore.getState().updateState;

    cookies.set('user',null)
    updateState({user:{}});
    // router.refresh();
}

export const getWeekName = (index:number)=>{
    var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    return days[index];
}



export const fieldValidation = (fields:{key:string,callback:Function}[])=>{
    let error = false;

    for (const item of fields) {
        if (!item?.key) {
            item.callback(true);
            error=true;
        }else{
            item.callback(false)
        }
    }

    return error;
}


export const getCountOfCertainItems = (list:Array<any>,key:string,value:any)=>{
    const newList:Array<any> = list.filter((item=>{
        return item[key] == value;
    }))

    return newList.length;
}