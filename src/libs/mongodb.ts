import ENV from "@/env";
import mongoose, { ConnectOptions } from "mongoose";

const clientOptions:ConnectOptions = { serverApi: { version: '1', strict: true, deprecationErrors: true}, };
const connectMongoDB = async()=>{
    try {
        await mongoose.connect(ENV.MONGO_URI,clientOptions)
    } catch (error) {
        console.log("error while connecting to db",error)
    }
}

export default connectMongoDB;