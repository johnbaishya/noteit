"use client"
import AppHeader from "@/components/AppBar";
import BackdropLoader from "@/components/BackdropLoader";
import EmailSetter from "@/components/EmailSetter";
import Navbar from "@/components/Navbar";
import NotesList from "@/components/NoteList";
import SplashScreen from "@/components/SplashScreen";
import TabNavigator from "@/components/TabNavigator";
import useAppStore from "@/zustand/store";
import { useEffect, useState } from "react";
import Cookies from "universal-cookie";

export default function Home() {
  const [hasUser,setHasUser] = useState(false);
  const [loading,setLoading] = useState(false);
  const {updateState,user:storeUser,appLoader,appInitializing} = useAppStore();
  
  
  const getCookieData = ()=>{
    setLoading(true);
    const cookies = new Cookies();
    const user =  cookies.get("user");
    updateState({user})
    let userAvailable = user?._id!=null;
    setHasUser(userAvailable);
    updateState({
      appInitializing:false,
    })
  }

  useEffect(()=>{
    getCookieData();
  },[])

  let userAvailable = storeUser?._id!=null;
  if(appInitializing){
    return(
      <SplashScreen/>
    )
  }
  return (
    <>
    {
     userAvailable?
      <>
        <TabNavigator/>
        {/* <NotesList/> */}
      </>
      :
      <EmailSetter/>
    }
    </>
  );
}
