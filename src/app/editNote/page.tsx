"use client";
import ENV from '@/env';
import useAppStore from '@/zustand/store'
import { Button, Card, CardActions, CardContent, IconButton, InputAdornment, OutlinedInput, TextField } from '@mui/material'
import axios from 'axios';
import moment from 'moment';
import { useRouter } from 'next/navigation';
import React, { useState } from 'react'
import ArrowBack from "@mui/icons-material/ArrowBack";
import { saveNote } from '@/zustand/action';
import { fieldValidation } from '@/common/methods';

export default function EditNote() {
    const router = useRouter();
    const {currentNote,updateState,user} = useAppStore();
    const [titleError,setTitleError] = useState(false);
    const updateField = (key:string,value:string)=>{
        updateState({
            currentNote:{
                ...currentNote,
                [key]:value,
            }
        })
    }


    const addNote = ()=>{
        let vError = fieldValidation([
            {
                key:currentNote.title,
                callback:(val:boolean)=>{setTitleError(val)}
            }

        ]);

        if(vError){
            return;
        }
        saveNote().then(res=>{
            router.push("/");
        })
    }

  return (
    <div className="mt-10 " >
        <IconButton color='inherit' className='mb-5' onClick={()=>{router.back()}}>
            <ArrowBack fontSize='large'/>
        </IconButton>
        <Card className="bg-[url('/images/bg4.jpg')]">
            <CardContent>
                <p 
                    style={{
                        fontSize:14, 
                        marginBottom:10,
                        float:"right"
                    }}
                >
                    created {moment(currentNote.createdAt).fromNow()}
                </p>
            <TextField
                required
                error = {titleError}
                helperText={titleError&&"required"}
                variant='standard'
                inputProps={{style: {fontSize: 24, fontWeight:'bold'}}} 
                InputLabelProps={{style:{fontSize:18}}}
                style={{width:"100%",marginBottom:10,fontSize:50}}
                id=""
                label="Title"
                value={currentNote.title}
                // placeholder='Title'
                onChange={(e)=>{
                    updateField("title",e.target.value);
                }}
            />
            <TextField
                
                variant='standard'
                style={{width:"100%"}}
                id="outlined-multiline-static"
                placeholder='wite your text here ....'
                // label="Text"
                multiline
                minRows={10}
                value={currentNote.text}
                onChange={(e)=>{
                    updateField("text",e.target.value);
                }}
            />
            </CardContent>
            <CardActions>
                <Button
                    variant='contained'
                    color='success'
                    onClick={addNote}
                >
                    Save
                </Button>
            </CardActions>
        </Card>
    </div>
  )
}
