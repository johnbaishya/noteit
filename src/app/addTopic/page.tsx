"use client";

import ENV from "@/env";
import connectMongoDB from "@/libs/mongodb";
import Topic from "@/models/topic";
import axios from "axios";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { saveTopic } from "../serverActions/topicController";


export default function AddTopic() {
  const [title,setTitle] = useState('');
  const [description,setDescription] = useState("");
  const router = useRouter();

  const handleSubmit = (e:any)=>{
    e.preventDefault();
    if(!title||!description){
      alert('Title and description are required');
      return;
    }

    axios.post(ENV.API_URI+"topics",{title,description})
    .then(res=>{
      router.push("/")
    })
    .catch(err=>{
      console.log("error while addibng topic",err);
    })
  }

  
  return (
    <form className='flex flex-col gap-3' action={saveTopic}> 
        <input type="text"  placeholder='Topic Title' className='border border-slate-500 px-8 py-2'
          onChange={(e)=>{
            setTitle(e.target.value)
          }}
          value={title}
          name="title"
        />
        <input type="text"  placeholder='Topic Description' className='border border-slate-500 px-8 py-2'
          onChange={(e)=>{
            setDescription(e.target.value)
          }}
          value={description}
          name="description"
        />
        <button className='bg-green-600 font-bold text-white py-3 px-6 w-fit' type="submit"> Add Topic</button>
    </form>
  )
}
