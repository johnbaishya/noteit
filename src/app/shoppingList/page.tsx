"use client";
import ENV from '@/env';
import useAppStore from '@/zustand/store'
import { Box, Button, Card, CardActions, CardContent, IconButton, InputAdornment, OutlinedInput, Skeleton, TextField, Typography } from '@mui/material'
import axios from 'axios';
import moment from 'moment';
import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import ArrowBack from "@mui/icons-material/ArrowBack";
import { getShoppingItems, saveNote, saveShoppingItem, saveShoppingList } from '@/zustand/action';
import { fieldValidation } from '@/common/methods';
import EachShoppingItem from '@/components/EachShoppingItem';
import { HiPencilAlt } from 'react-icons/hi';
import AppContentLoader from '@/components/AppContentLoader';

export default function EditNote() {
    const router = useRouter();
    const {updateState,user,currentShoppingList,allShoppingList,shoppingItems,shoppingItemsLoading} = useAppStore();
    const [titleError,setTitleError] = useState(false);
    const [titleEditable,setTitleEditable] = useState(false);
    const [itemNameError,setItemNameError] = useState(false);
    const [itemName,setItemName] = useState("");
    
    const newList = !currentShoppingList._id;
    
    useEffect(()=>{
        if(newList){
            setTitleEditable(true);
        }else{
            getShoppingItems();
            setTitleEditable(false)
        }
    },[newList])
    
    const updateField = (key:string,value:string)=>{
        updateState({
            currentShoppingList:{
                ...currentShoppingList,
                [key]:value,
            }
        })
    }
    


    const updateShoppingListTitle = ()=>{
        let vError = fieldValidation([
            {
                key:currentShoppingList.title,
                callback:(val:boolean)=>{setTitleError(val)}
            }
        ]);

        if(vError){
            return;
        }
        
        saveShoppingList().then(res=>{
            // router.push("/");
            console.log(res);
            setTitleEditable(false);
        })
    }



    const addShoppingItem = ()=>{
        let vError = fieldValidation([
            {
                key:itemName,
                callback:(val:boolean)=>{setItemNameError(val)}
            }
        ]);

        if(vError){
            return
        }

        saveShoppingItem({title:itemName},null).then(res=>{
            setItemName("");
            setItemNameError(false);
        })
    }

    return (
    <div className='mt-10'>
        <IconButton color='inherit' className='mb-5' onClick={()=>{router.back()}}>
            <ArrowBack fontSize='large'/>
        </IconButton>
        <Card>
            <CardContent>
                {/* <p 
                    style={{
                        fontSize:14, 
                        marginBottom:10,
                        float:"right"
                    }}
                >
                    created {moment(currentNote.createdAt).fromNow()}
                </p> */}
                    {!titleEditable?
                    <>
                        <div className='flex content-center align-middle mb-2'>
                            <Typography variant='h5' className='leading-tight flex-1 border-b-2'>
                                {currentShoppingList.title}
                            </Typography>
                            <IconButton 
                                onClick={()=>{
                                    setTitleEditable(true);
                                }}
                                >
                                <HiPencilAlt size={24} className='text-cyan-600'/>
                            </IconButton>
                        </div>
                    </>:
                    <>
                        <div className='flex items-center'>
                            <TextField
                                required
                                error = {titleError}
                                helperText={titleError&&"required"}
                                variant='standard'
                                inputProps={{style: {fontSize: 24, fontWeight:'bold'}}} 
                                InputLabelProps={{style:{fontSize:18}}}
                                style={{width:"100%",marginBottom:10,fontSize:50}}
                                id=""
                                label="Shopping List Title"
                                value={currentShoppingList.title}
                                // placeholder='Title'
                                onChange={(e)=>{
                                    updateField("title",e.target.value);
                                }}
                            />
                            <Button
                                    variant='contained'
                                    color='success'
                                    // size='small'
                                    // className='h-10'
                                    onClick={updateShoppingListTitle}
                                >
                                Save
                            </Button>
                        </div>
                    </>
                    }
                    {shoppingItemsLoading&&
                    <AppContentLoader/>
                    }
                    {
                        shoppingItems.map((item:any,index:number)=>{
                            return(
                                <EachShoppingItem data={item} key={index} sn={index}/>
                            )
                        })
                    }
            
            </CardContent>
            {!newList&&
            <CardActions
                className='flex'
            >
                <TextField
                className='flex-1'
                size='small'
                error = {titleError}
                variant='outlined'
                id=""
                placeholder='shopping item ..'
                value={itemName}
                onChange={(e)=>{
                    setItemName(e.target.value)
                }}
            />
                <Button
                    variant='contained'
                    color='success'
                    onClick={addShoppingItem}
                >
                    Add Item
                </Button>
            </CardActions>}
        </Card>
    </div>
  )
}
