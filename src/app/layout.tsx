import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Navbar from "@/components/Navbar";
import AppHeader from "@/components/AppBar";
import BackdropLoader from "@/components/BackdropLoader";
import Feedback from "@/components/Feedback";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Note It",
  description: "An App to save your notes, shopping list, reminders and tasks",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        {/* <div className="max-w-4xl mx-auto p-4"> */}
          {/* <Navbar/> */}
          {/* <AppHeader/> */}
          <AppHeader/>
          <div className=" "> 

            <div className="max-w-4xl mx-auto">
              {children}
            </div>
          </div>
          {/* <BackdropLoader/> */}
          <Feedback/>
        {/* </div> */}
        </body>
    </html>
  );
}
