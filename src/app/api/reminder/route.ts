import connectMongoDB from "@/libs/mongodb";
import { Reminder, User } from "@/models/models";
import { NextRequest, NextResponse } from "next/server";


export const POST = async(request:NextRequest)=>{
    const {title,description,appointment_date,time,completed,user_id} = await request.json();
    const id = request.nextUrl.searchParams.get("id");
    try {
        
        await connectMongoDB();
        let user = await User.findById(user_id);
        if(!user){
            return NextResponse.json({message:"user not found"},{status:500})
        }
        let reminder;
        if(!id){
            reminder  = await Reminder.create({title,description,appointment_date,time,completed,user});
        }else{
            await Reminder.findByIdAndUpdate(id,{title,description,appointment_date,time,completed,user});
            reminder = await Reminder.findById(id);
        }
        return NextResponse.json(reminder,{status:201});
    } catch (error) {
        return NextResponse.json(error,{status:500}) 
    }
}

export const GET = async(request:NextRequest)=>{ 
    const userId = request.nextUrl.searchParams.get("user_id");
    await connectMongoDB();
    let data = [];
    if(userId){
        data  = await Reminder.find({user:userId}).sort('appointment_date')
    }
    return NextResponse.json(data,{status:200})
}