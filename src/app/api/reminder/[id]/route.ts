import connectMongoDB from "@/libs/mongodb";
import { Reminder} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export async function PUT(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    const {title,description,completed,date,schedule} = await request.json();
    await connectMongoDB();
    await Reminder.findByIdAndUpdate(id,{title,description,completed,date,schedule});
    const data = await Reminder.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function GET(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    await connectMongoDB();
    const data = await Reminder.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function DELETE(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params
    await connectMongoDB();
    await Reminder.findByIdAndDelete(id);
    return NextResponse.json({message:"Note deleted"},{status:200})
}