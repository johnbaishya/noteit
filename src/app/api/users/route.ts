import connectMongoDB from "@/libs/mongodb";
import {Note, User} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export const POST = async(request:NextRequest)=>{
    const {email} = await request.json();
    await connectMongoDB();
    let user  = await User.findOne({email})
    if(!user){
        user = await User.create({email})
    }
    // await User.create({email});
    return NextResponse.json(user,{status:201});
}

export async function GET(){
    await connectMongoDB();
    const users  = await User.find();
    return NextResponse.json(users)

}

// export async function DELETE(request:NextRequest){
//     const id = request.nextUrl.searchParams.get("id");
//     await connectMongoDB();
//     await Topic.findByIdAndDelete(id);
//     return NextResponse.json({message:"Topic deleted"},{status:200})
// }