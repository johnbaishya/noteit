import connectMongoDB from "@/libs/mongodb";
import { Note, User } from "@/models/models";
import mongoose from "mongoose";
import { NextRequest, NextResponse } from "next/server";

export const POST = async(request:NextRequest)=>{
    const {title,user_id,text} = await request.json();
    const id = request.nextUrl.searchParams.get("id");
    await connectMongoDB();
    let user = await User.findById(user_id);
    if(!user){
        return NextResponse.json({message:"user not found"},{status:500})
    }
    let note;
    if(!id){
        note  = await Note.create({title,text,user});
    }
    else{
        await Note.findByIdAndUpdate(id,{title,text});
        note = await Note.findById(id);
    }
    return NextResponse.json(note,{status:200});
    // return NextResponse.json({user},{status:201})

}

export const GET = async(request:NextRequest)=>{ 
    const userId = request.nextUrl.searchParams.get("user_id");
    await connectMongoDB();
    let notes = [];
    if(userId){
        notes  = (await Note.find({user:userId})).reverse();
    }
    return NextResponse.json(notes,{status:200})

}