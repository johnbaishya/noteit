import connectMongoDB from "@/libs/mongodb";
import { Note } from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export async function PUT(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    const {title,text} = await request.json();
    await connectMongoDB();
    await Note.findByIdAndUpdate(id,{title,text});
    const note = await Note.findById(id);
    return NextResponse.json(note,{status:200});
}

export async function GET(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    await connectMongoDB();
    const note = await Note.findById(id);
    return NextResponse.json(note,{status:200});
}

export async function DELETE(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params
    await connectMongoDB();
    await Note.findByIdAndDelete(id);
    return NextResponse.json({message:"Note deleted"},{status:200})
}