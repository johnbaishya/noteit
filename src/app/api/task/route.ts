import connectMongoDB from "@/libs/mongodb";
import { Task } from "@/models/models";
import { NextRequest, NextResponse } from "next/server";


export const POST = async(request:NextRequest)=>{
    const {title,description,completed,date,schedule} = await request.json();
    const id = request.nextUrl.searchParams.get("id");

    try {
        
        await connectMongoDB();
        let item;
        if(!id){
            item  = await Task.create({title,description,completed,date,schedule});
        }else{
            await Task.findByIdAndUpdate(id,{title,description,completed,date,schedule});
            item = await Task.findById(id);
        }
        return NextResponse.json(item,{status:201});
    } catch (error) {
        return NextResponse.json(error,{status:500}) 
    }


    // await connectMongoDB();
    // const data  = await Task.create({title,description,completed,date,schedule});
    // return NextResponse.json(data,{status:201});
}

export const GET = async(request:NextRequest)=>{ 
    const scheduleId = request.nextUrl.searchParams.get("schedule_id");
    await connectMongoDB();
    let data = [];
    if(scheduleId){
        data  = await Task.find({schedule:scheduleId})
    }
    return NextResponse.json(data,{status:200})
}