import connectMongoDB from "@/libs/mongodb";
import { ShoppingItem} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export async function PUT(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    const {title,description,expected_price,actual_price,bought,boughtDate,category,shoppingList} = await request.json();
    await connectMongoDB();
    await ShoppingItem.findByIdAndUpdate(id,{title,description,expected_price,actual_price,bought,boughtDate,category,shoppingList});
    const data = await ShoppingItem.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function GET(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    await connectMongoDB();
    const data = await ShoppingItem.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function DELETE(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params
    await connectMongoDB();
    await ShoppingItem.findByIdAndDelete(id);
    return NextResponse.json({message:"Note deleted"},{status:200})
}