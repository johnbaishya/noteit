import connectMongoDB from "@/libs/mongodb";
import {ShoppingItem} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";


export const POST = async(request:NextRequest)=>{
    const {title,description,expected_price,actual_price,bought,boughtDate,category,shoppingList} = await request.json();
    const id = request.nextUrl.searchParams.get("id");
    try {
        
        await connectMongoDB();
        let item;
        if(!id){
            item  = await ShoppingItem.create({title,description,expected_price,actual_price,bought,boughtDate,category,shoppingList});
        }else{
            await ShoppingItem.findByIdAndUpdate(id,{title,description,expected_price,actual_price,bought,boughtDate,category,shoppingList});
            item = await ShoppingItem.findById(id);
        }
        return NextResponse.json(item,{status:201});
    } catch (error) {
        return NextResponse.json(error,{status:500}) 
    }
    // const data  = await ShoppingItem.create({title,description,expected_price,actual_price,bought,boughtDate,category,shoppingList});
    // return NextResponse.json(data,{status:201});
}

export const GET = async(request:NextRequest)=>{ 
    const listId = request.nextUrl.searchParams.get("shopping_list_id");
    await connectMongoDB();
    let data = [];
    if(listId){
        data  = await ShoppingItem.find({shoppingList:listId})
    }
    return NextResponse.json(data,{status:200})
}