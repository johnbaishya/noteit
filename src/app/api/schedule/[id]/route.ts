import connectMongoDB from "@/libs/mongodb";
import {Schedule} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export async function PUT(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    const {title,description,date,user} = await request.json();
    await connectMongoDB();
    await Schedule.findByIdAndUpdate(id,{title,description,date,user});
    const data = await Schedule.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function GET(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    await connectMongoDB();
    const data = await Schedule.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function DELETE(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params
    await connectMongoDB();
    await Schedule.findByIdAndDelete(id);
    return NextResponse.json({message:"Note deleted"},{status:200})
}