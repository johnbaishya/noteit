import connectMongoDB from "@/libs/mongodb";
import {Schedule} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";


export const POST = async(request:NextRequest)=>{
    const {title,description,date,user} = await request.json();
    const id = request.nextUrl.searchParams.get("id");


    try {
        
        await connectMongoDB();
        let item;
        if(!id){
            item  = await Schedule.create({title,description,date,user});
        }else{
            await Schedule.findByIdAndUpdate(id,{title,description,date,user});
            item = await Schedule.findById(id);
        }
        return NextResponse.json(item,{status:201});
    } catch (error) {
        return NextResponse.json(error,{status:500}) 
    }

    // await connectMongoDB();
    // const data  = await Schedule.create({title,description,date,user});
    // return NextResponse.json(data,{status:201});
}

export const GET = async(request:NextRequest)=>{ 
    const userId = request.nextUrl.searchParams.get("user_id");
    await connectMongoDB();
    let data = [];
    if(userId){
        data  = await Schedule.find({user:userId})
    }
    return NextResponse.json(data,{status:200})
}