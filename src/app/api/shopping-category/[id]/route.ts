import connectMongoDB from "@/libs/mongodb";
import {ShoppingCategory } from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export async function PUT(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    const {name,description}:{name:string,description:string} = await request.json();
    await connectMongoDB();
    await ShoppingCategory.findByIdAndUpdate(id,{name,description});
    const shoppingCategory = await ShoppingCategory.findById(id);
    return NextResponse.json(shoppingCategory,{status:200});
}

export async function GET(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    await connectMongoDB();
    const shoppingCategory = await ShoppingCategory.findById(id);
    return NextResponse.json(shoppingCategory,{status:200});
}

export async function DELETE(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params
    await connectMongoDB();
    await ShoppingCategory.findByIdAndDelete(id);
    return NextResponse.json({message:"Note deleted"},{status:200})
}