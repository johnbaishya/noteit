import connectMongoDB from "@/libs/mongodb";
import {ShoppingCategory} from "@/models/models";
import { NextRequest, NextResponse } from "next/server";


export const POST = async(request:NextRequest)=>{
    const {name,description} = await request.json();
    await connectMongoDB();
    const shoppingCategory  = await ShoppingCategory.create({name,description});
    return NextResponse.json(shoppingCategory,{status:201});
}

export const GET = async(request:NextRequest)=>{ 
    const userId = request.nextUrl.searchParams.get("user_id");
    await connectMongoDB();
    const shoppingCategories  = await ShoppingCategory.find()
    return NextResponse.json(shoppingCategories,{status:200})
}