import connectMongoDB from "@/libs/mongodb";
import { ShoppingList } from "@/models/models";
import { NextRequest, NextResponse } from "next/server";

export async function PUT(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    const {title,description,category,user} = await request.json();
    await connectMongoDB();
    await ShoppingList.findByIdAndUpdate(id,{title,description,category,user});
    const data = await ShoppingList.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function GET(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params;
    await connectMongoDB();
    const data = await ShoppingList.findById(id);
    return NextResponse.json(data,{status:200});
}

export async function DELETE(request:NextRequest,{params}:{params:{id:string}}){
    const {id} = params
    await connectMongoDB();
    await ShoppingList.findByIdAndDelete(id);
    return NextResponse.json({message:"Note deleted"},{status:200})
}