import connectMongoDB from "@/libs/mongodb";
import {ShoppingItem, ShoppingList, User} from "@/models/models";
import mongoose from "mongoose";
import { NextRequest, NextResponse } from "next/server";


export const POST = async(request:NextRequest)=>{
    const {title,description,category,user_id} = await request.json();
    const id = request.nextUrl.searchParams.get("id");

    try {
        await connectMongoDB();
        let user = await User.findById(user_id);
        if(!user){
            return NextResponse.json({message:"user not found"},{status:500})
        }
        let shoppingList;
        if(!id){
            shoppingList  = await ShoppingList.create({title,description,category,user});
        }else{
            await ShoppingList.findByIdAndUpdate(id,{title,description,category,user});
            shoppingList = await ShoppingList.findById(id);
        }
        return NextResponse.json(shoppingList,{status:201});
    } catch (error) {
        return NextResponse.json(error,{status:500}) 
    }
}

export const GET = async(request:NextRequest)=>{ 
    try {
        
        const userId = request.nextUrl.searchParams.get("user_id");
        await connectMongoDB();
        let data:any = [];
        let newData:any=[];
        if(userId){
            data  = await ShoppingList.find({user:userId}).populate("items")
            // for (const item of data) {
            //     let sitem = (await ShoppingItem.find({shoppingList:item._id})).length;
            //     let citem = (await ShoppingItem.find({shoppingList:item._id,bought:true})).length;
            //     let obj = item.toObject();
            //     obj.totalItems = sitem;
            //     obj.completedItems=citem;
            //     newData.push(obj)
            // }
          
        }
        return NextResponse.json(data,{status:200})
    } catch (error) {
        console.log(error)
        return NextResponse.json(error,{status:500})
    }
}