import EditTopicForm from "@/components/EditTopicForm";

export default function EditTopic({params}:{params:{id:string}}){
    const {id} = params;
    return(
        <EditTopicForm/>
    )
}