"use client";
import EmailSetter from "@/components/EmailSetter";
import Navbar from "@/components/Navbar";
import TopicsList from "@/components/TopicsList";
import Cookies from 'universal-cookie';


export default function App(){
    const cookies = new Cookies(null, { path: '/' });
    const user  = cookies.get('user');
    return(
        <>
        {
            user?._id?
            <>
                {/* <Navbar/>
                <TopicsList/> */}
            </>:
            <EmailSetter/>
        }
        </>
    )
}