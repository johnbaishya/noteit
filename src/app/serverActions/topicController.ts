"use server";
import connectMongoDB from "@/libs/mongodb";
import Topic from "@/models/topic";
import { revalidatePath } from "next/cache";
import { redirect } from "next/navigation";

export const saveTopic = async (fd:FormData)=>{
    await connectMongoDB();
    await Topic.create({title:fd.get("title"),description:fd.get("description")})
    revalidatePath("/")
    redirect("/");
  }