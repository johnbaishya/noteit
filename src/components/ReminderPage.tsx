import { Box, Button, Card, Skeleton, TextField } from '@mui/material'
import React, { Key, useEffect, useState } from 'react'
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import moment from 'moment';
import dayjs from 'dayjs';
import AddReminderModal from './AddReminderModal';
import EachReminder from './EachReminder';
import axios from 'axios';
import ENV from '@/env';
import useAppStore from '@/zustand/store';
import { getReminderList } from '@/zustand/action';

export default function ReminderPage() {
    const [showAdd,setShowAdd] = useState(false) 
    const {reminders,remindersLoading} = useAppStore();

    const getList = ()=>{
       getReminderList();
    }

    useEffect(()=>{
        getList();
    },[])
  return (
    <>
        <Box className = "content-center justify-center items-center flex">
            <Button variant='contained' style={{zIndex:0, marginBottom:10}} onClick={()=>{
                setShowAdd(true)
            }}>
            + Add New Reminder
            </Button>
        </Box>
        {remindersLoading&&
        <Box sx={{ width: "100%" }}>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
        </Box>}
        <Box>
            {
                reminders.map((item:any,index:Number)=>{
                    return(
                        <EachReminder key={index as Key} data={item} action1={()=>{setShowAdd(true)}}/>
                    )
                })
            }
        </Box>
        <AddReminderModal open={showAdd} close={()=>{setShowAdd(false)}} refreshList={getList}/>
    </>
  )
}
