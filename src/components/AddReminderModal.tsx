import { Box, Button, Card, Dialog, DialogContent, DialogTitle, TextField } from '@mui/material'
import { DatePicker } from '@mui/x-date-pickers/DatePicker'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { TimePicker } from '@mui/x-date-pickers/TimePicker'
import { DemoContainer } from '@mui/x-date-pickers/internals/demo'
import dayjs from 'dayjs'
import React, { useState } from 'react'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import axios from 'axios'
import useAppStore from '@/zustand/store'
import moment from 'moment'
import ENV from '@/env'
import { useRouter } from 'next/navigation'
import { saveReminder } from '@/zustand/action'
import BackdropLoader from './BackdropLoader'
import { fieldValidation } from '@/common/methods'

export default function AddReminderModal({open,close,refreshList}:{open:boolean,close:Function,refreshList:Function}) {
    const {currentReminder,updateState,user} = useAppStore();
    const [loading,setLoading] = useState(false);
    const {title,description,appointment_date,completed,time,vtime} = currentReminder;
    const [titleError,setTitleError] = useState(false);
    const router = useRouter();
    let noId = !currentReminder._id;
    let hasId = !noId; 
    
    const updateField = (data:any)=>{
        updateState({
            currentReminder:{
                ...currentReminder,
                ...data,
            }
        })
    }

    const updateTime = function(newValue:any){
        updateField({
            vtime:newValue?.toString(),
            time:newValue?.hour()+":"+newValue?.minute()
        })
    }

    const addReminder = ()=>{

        let vError = fieldValidation([
            {
                key:currentReminder.title,
                callback:(val:boolean)=>{setTitleError(val)}
            }

        ]);

        if(vError){
            return;
        }
        saveReminder().then(res=>{
            close();
        })
    }

    return (
        <Dialog
            open={open}
            onClose={()=>{
                updateState({currentReminder:{}})
                close()
            }}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
            {hasId?"Edit":"Add" } Your New Reminder
            </DialogTitle>
                <Box>
                    <Card 
                        className='p-10 pt-2'
                    >
                        
                        <TextField 
                            required
                            error={titleError}
                            helperText={titleError&&"Reminder title required"}
                            className='mb-5'
                            variant='standard'
                            inputProps={{style: {fontSize: 20, fontWeight:'bold'}}} 
                            InputLabelProps={{style:{fontSize:18}}}
                            style={{width:"100%",fontSize:50}}
                            id=""
                            label="Reminder Title"
                            value={title}
                            // placeholder='Title'
                            onChange={(e)=>{
                                updateField({"title":e.target.value});
                            }}
                            />
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DemoContainer components={['DatePicker', 'TimePicker']}>
                                {/* <DatePicker label="Uncontrolled picker" defaultValue={dayjs('2022-04-17')} /> */}
                                <DatePicker
                                label="Appointment Dater"
                                // value={appointment_date}
                                onChange={(newValue) => {
                                    updateField({"appointment_date":newValue?.toString()});
                                }}
                                value={dayjs(appointment_date)}
                                />
                                <TimePicker
                                label="Appointed Time"
                                value={dayjs(vtime)}
                                // value={time}
                                onChange={(newValue:any) => {
                                   updateTime(newValue)
                                }}
                                />
                            </DemoContainer>
                        </LocalizationProvider>
                        <TextField 
                        variant='outlined'
                        // inputProps={{style: {fontSize: 24, fontWeight:'bold'}}} 
                        // InputLabelProps={{style:{fontSize:18}}}
                        style={{width:"100%",marginBottom:10,fontSize:50}}
                        id=""
                        // label="Description"
                        multiline
                        rows={3}
                        className='mt-5'
                        placeholder='add more description here .... '
                        value={description}
                        // placeholder='Title'
                        onChange={(e)=>{
                            updateField({"description":e.target.value});
                        }}
                        />
                        <div className='flex justify-end gap-4'>
                            <Button
                                variant='contained'
                                color='success'
                                onClick={()=>{addReminder()}}
                            >
                                {hasId?"Update Reminder":"+ Add Reminder"}
                            </Button>
                            <Button
                                variant='contained'
                                color='inherit'
                                onClick={()=>{
                                    updateState({currentReminder:{}})
                                    close()
                                }}
                            >
                                Cancel
                            </Button>
                        </div>
                    </Card>
                </Box>

                <BackdropLoader/>
        </Dialog>
    )
}
