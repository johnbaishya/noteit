import * as React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import NotesList from './NoteList';
import ReminderPage from './ReminderPage';
import AllShoppingList from './AllShoppingList';
import ScheduleList from './SceduleList';
import useAppStore from '@/zustand/store';
import { updateStore } from '@/zustand/action';

export default function TabNavigator() {
  const {mainTabValue} = useAppStore();
  const [value, setValue] = React.useState('1');

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    // setValue(newValue);
    updateStore({
      mainTabValue:newValue
    })
  };

  return (
    <Box sx={{ width: '100%', typography: 'body1',}}>
      <TabContext value={mainTabValue}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider',position:"sticky", top:60, backgroundColor:"white", height:60, zIndex:1}}>
            <div style={{}}>
                <div style={{}}>
                    <TabList onChange={handleChange} aria-label="lab API tabs example" centered style={{height:60, alignItems:'center'}}> 
                        <Tab label="Notes" value="1" />
                        <Tab label="Reminders" value="2" />
                        <Tab label="Shopping List" value="3" />
                        <Tab label="Schedule" value="4" />
                    </TabList>
                </div>
            </div>
        </Box>
        <TabPanel value="1">
            <NotesList/>
        </TabPanel>
        <TabPanel value="2">
          <ReminderPage/>
        </TabPanel>
        <TabPanel value="3">
          <AllShoppingList/>
        </TabPanel>
        <TabPanel value="4">
          <ScheduleList/>
        </TabPanel>
      </TabContext>
    </Box>
  );
}
