import { Box, Skeleton } from '@mui/material'
import React, { useEffect, useState } from 'react'

export default function AppContentLoader() {
    
    const [show,setShow] = useState(false)

    useEffect(()=>{
        setTimeout(()=>{
            setShow(true)
        },300)
    },[])

    if(!show){
        return null;
    }
  return (
    <Box sx={{ width: "100%" }}>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
        
    </Box>
  )
}
