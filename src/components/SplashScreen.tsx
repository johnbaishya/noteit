import { Box, Card, CircularProgress, Typography } from '@mui/material'
import React from 'react'

export default function SplashScreen() {
  return (
    <div className="p-10 mt-40 flex-col space-y-5">
                   <Typography variant='h2' className='text-center text-sky-600 ' gutterBottom >
                        NoteIT
                    </Typography>
                    <Box className = "flex justify-center space-x-5" >
                   <CircularProgress color='primary' size={90}/>
                    {/* <TextField id="outlined-basic" label="email" variant="outlined" className='flex-1 bg-white'
                        onChange={(e)=>{
                            setEmail(e.target.value)
                        }}
                        value={email}
                    />
                    <Button onClick={saveEmail} variant='contained' color='success'>Continue</Button> */}
                </Box>
        </div>
  )
}
