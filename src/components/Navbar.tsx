import useAppStore from "@/zustand/store";
import { Button } from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/navigation";
import Cookies from "universal-cookie";

export default function Navbar(){
    const cookies = new Cookies(null, { path: '/' });
    const router = useRouter();
    const {updateState} = useAppStore();

    const logout  = ()=>{
        cookies.set('user',null)
        updateState({user:{}});
    }
    return(
        <nav className="flex justify-between items-center bg-slate-800 px-8 py-3">
            <Link className="text-white font-bold" href={"/"}>Gt coding</Link>
            <Link className="bg-white p-2" href={"/addTopic"}>Add Topic</Link>
            <Button
                onClick={logout}
            >
                log 
            </Button>
        </nav>
    )
}