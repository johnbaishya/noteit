import React from 'react'
// import RemoveBtn from './RemoveBtn'
import Link from 'next/link'
import {HiPencilAlt} from "react-icons/hi"
import ENV from '@/env'

const getTopics = async()=>{
    try {
        const res  = await fetch(ENV.API_URI+"topics",{
            cache:"no-store",
        });

        if(!res.ok){
            throw new Error("Failed to fetch Topics")
        }

        return res.json();
    } catch (error) {
        console.log("error loading topics",error);
    }
}

export default async function TopicsList() {
    const {topics} = await getTopics();
  return (
    <>
    {
        topics.map((item:any,index:number)=>(
        <div className='p-4 border-slate-500 my-3 flex justify-between gap-5 items-start' key={index}>
            <div>
                <h2 className='font-bold text-2xl'>{item.title}</h2>
                <div>{item.description}</div>
            </div>
            <div className='flex gap-2'>
                {/* <RemoveBtn id= {item.id}/> */}
                <Link href={"/editTopic/"+item._id}>
                    <HiPencilAlt size={24}/>
                </Link>
            </div>
        </div>
        ))
    }
    </>
  )
}
