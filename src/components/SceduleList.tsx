import React, { useEffect } from 'react'
import EachShoppingList from './EachShoppingList'
import { Box, Button, Skeleton } from '@mui/material'
import Link from 'next/link'
import useAppStore from '@/zustand/store'
import { getSchedules, updateStore } from '@/zustand/action'
import EachSchedule from './EachSchedule'

export default function ScheduleList() {
    const {schedules,schedulesLoading} = useAppStore();

    useEffect(()=>{
        getSchedules();
        updateStore({
            currentSchedule:{},
            tasks:[],
        })
    },[])
  return (
    <>
        <Box className = "content-center justify-center items-center flex">
            <Button variant='contained' style={{zIndex:0, marginBottom:10}} onClick={()=>{
                // setShowAdd(true)
            }}>
                <Link href={"/schedule"}>
            + Add New Schedule
                </Link>
            </Button>
        </Box>
        {schedulesLoading&&
        <Box sx={{ width: "100%" }}>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
        </Box>}
        {
            schedules.map((item:any,index:number)=>{
                return(
                    <EachSchedule key={index} data={item}/>
                )
            })
        }
    </>
  )
}
