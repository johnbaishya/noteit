import React, { useEffect } from 'react'
import EachShoppingList from './EachShoppingList'
import { Box, Button, Skeleton } from '@mui/material'
import Link from 'next/link'
import useAppStore from '@/zustand/store'
import { getAllShoppingList, updateStore } from '@/zustand/action'

export default function AllShoppingList() {
    const {allShoppingList,shoppingListLoading} = useAppStore();

    useEffect(()=>{
        getAllShoppingList();
        updateStore({
            currentShoppingList:{},
            shoppingItems:[],
        })
    },[])
    console.log(allShoppingList);
  return (
    <>
        <Box className = "content-center justify-center items-center flex">
            <Button variant='contained' style={{zIndex:0, marginBottom:10}} onClick={()=>{
                // setShowAdd(true)
            }}>
                <Link href={"/shoppingList"}>
            + Add New Shopping List
                </Link>
            </Button>
        </Box>
        {shoppingListLoading&&
        <Box sx={{ width: "100%" }}>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
            <Skeleton  height={80}/>
        </Box>}
        {
            allShoppingList.map((item:any,index:number)=>{
                return(
                    <EachShoppingList key={index} data={item}/>
                )
            })
        }
    </>
  )
}
