import { getWeekName } from '@/common/methods'
import { deleteItem, getReminderList, updateReminderCompleted } from '@/zustand/action'
import useAppStore from '@/zustand/store'
import { Box, Card, Checkbox, Grid, IconButton, Typography } from '@mui/material'
import dayjs from 'dayjs'
import moment from 'moment'
import React from 'react'
import { HiCheckCircle, HiOutlineTrash, HiPencilAlt } from 'react-icons/hi'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';

type ReminderData = {
  _id:string,
  title:String,
  description:String,
  appointment_date:Date,
  completed:boolean,
  time:Date,
  user:String,
}


export default function EachReminder({data,action1}:{data:ReminderData,action1:Function}) {

  const {updateState} = useAppStore(); 

  return (
    <Box className="flex gap-3 border-t-2 mt-1">
      <div className='bg-slate-400 h-20 rounded w-28 text-center pt-2 align-middle'>
        <div
            className='text-gray-200 font-bold'
          >
              {getWeekName(moment(data.appointment_date).get("weekday"))}
          </div>
          <div
            className='text-gray-100 font-sans text-sm'
          >
              {moment(data.appointment_date).format("MMM DD")}
          </div>
          {data.time&&
            <div className='flex justify-center font-bold text-white'>
              {moment(data.time,"HH:mm").format("hh:mm")}{" "}
              {/* {data.time} */}
            {/* <Typography variant='subtitle2' className='text-gray-60 text-sm align-text-bottom'>
              {" "+moment(data.time,"HH:mm").format("a")}
            </Typography> */}
          </div>}

      </div>
      <Card className='p-3 mb-1 flex-1'>
        <div className='flex justify-between'>
          <div>
            <Typography variant='h6'>
                {data.title}
            </Typography>
            <Typography variant='subtitle2'>
                {data.description}
            </Typography>
          </div>
          <div>
          <Checkbox
            checked={data.completed}
            color='success'

            onChange={(res)=>{
              
              updateReminderCompleted(data._id,res.target.checked)
            }}
            icon={<RadioButtonUncheckedIcon />}
            checkedIcon={<CheckCircleIcon />}
          />

            <IconButton
              onClick={()=>{deleteItem(data._id,"reminder","Reminder", getReminderList)}}
            >
              <HiOutlineTrash
                  className='text-red-500'
                  size={24}

              />
            </IconButton>
            <IconButton 
                onClick={()=>{
                    updateState({currentReminder:data})
                    action1();
                }}
            >
                <HiPencilAlt size={24} className='text-cyan-600'/>
            </IconButton>
          </div>
        </div>
      </Card>
    </Box>
  )
}
