import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import {HiOutlineTrash, HiPencilAlt} from "react-icons/hi"
import ENV from '@/env'
import axios from 'axios'
import useAppStore from '@/zustand/store'
import moment from "moment";
import { useRouter } from 'next/navigation'
import {Box, Button, CircularProgress, Grid, IconButton, Skeleton } from '@mui/material'
import { deleteItem, getNoteList } from "@/zustand/action";



export default function NotesList() {
    const router = useRouter();
    // const [notes,setNotes] = useState([]);
    const [loading,setLoading] = useState(false);
    const {user,updateState,notes,notesLoading} = useAppStore();

    const getNotes = ()=>{
        getNoteList();
    }

    const deleteNote = (id:string)=>{
        deleteItem(id,"notes","Note", getNoteList)
    }

    

    const editNote = (id:string)=>{
        updateState({appLoader:true})
        axios.get(ENV.API_URI+"notes/"+id)
        .then(res=>{
            console.log(res.data);
            updateState({
                currentNote:res.data,
                appLoader:false,
            });
            router.push("/editNote");
            updateState({appLoader:false})
        })
    }

    const addNote = ()=>{
        updateState({
            currentNote:{},
        })
        router.push("/editNote");
    }

    useEffect(()=>{
        getNotes();
    },[])
    
  return (
    <>
    <Box className = "content-center justify-center items-center flex">
            <Button variant='contained' style={{zIndex:0, marginBottom:10}} onClick={()=>{
                addNote();
            }}>
            + Add New Note
            </Button>
        </Box>
    {notesLoading&&
    <Box sx={{ width: "100%" }}>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
        <Skeleton  height={80}/>
    </Box>}
    {
        notes.map((item:any,index:number)=>{
            const formattedDate = moment(item.createdAt).format("MMM DD YYYY");
            let title = item.title?item.title:formattedDate;
            return(
                <div className="pl-5 border-slate-400 my-5 flex justify-between gap-5 items-start overflow-hidden 
                bg-[url('/images/bg5.jpg')] shadow-md" key={index}
                    style={{borderWidth:1, borderRadius:5}}
                >
                    <div className='flex-1 border-r border-red-400 py-5'>
                        <h2 className='font-bold text-2xl border-y border-cyan-500'>{title}</h2>
                        <p className='line-clamp-2 border-b border-cyan-500 pb-2'>{item.text}</p>
                    </div>
                    {/* <img src='/images/bg1.jpg' className='relative z-0'/> */}
                    <div className='flex gap-2'>
                        <IconButton
                            onClick={()=>{deleteNote(item._id)}}
                        >
                        <HiOutlineTrash
                            className='text-red-500'
                            size={24}

                        />
                        </IconButton>
                        <IconButton 
                            onClick={()=>{
                                editNote(item._id);
                            }}
                        >
                            <HiPencilAlt size={24} className='text-cyan-600'/>
                        </IconButton>
                    </div>
                </div>
                )
            }
        )
    }
    </>
  )
}
