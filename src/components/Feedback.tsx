"use client"
import React from 'react'
import BackdropLoader from './BackdropLoader'
import { Alert, Slide, Snackbar } from '@mui/material'
import useAppStore from '@/zustand/store'
import { updateStore } from '@/zustand/action'

export default function Feedback() {
    const {success,successMessage,error,errorMessage} = useAppStore();
    const closeSuccess = ()=>{
        updateStore({
            success:false,
        })
        setTimeout(()=>{
            updateStore({
                successMessage:"",
            })
        },1000)
    }
    const closeError = ()=>{
        updateStore({
            error:false,
        });

        setTimeout(()=>{
            updateStore({
                errorMessage:"",
            })
        },1000)
    }
    return (
        <div>
            <BackdropLoader/>
            <Snackbar open={success} autoHideDuration={2000} onClose={closeSuccess} TransitionComponent={Slide}
                anchorOrigin={{horizontal:"center", vertical:"bottom"}} 
            >
                <Alert
                onClose={closeSuccess}
                severity="success"
                variant="filled"
                sx={{ width: '100%' }}
                >
                {successMessage}
                </Alert>
            </Snackbar>



            <Snackbar 
                open={error} 
                autoHideDuration={2000} 
                onClose={closeError} 
                TransitionComponent={Slide}
                anchorOrigin={{horizontal:"center", vertical:"bottom"}} 
                >
                <Alert
                onClose={closeError}
                severity="error"
                variant="filled"
                sx={{ width: '100%' }}
                >
                {errorMessage}
                </Alert>
            </Snackbar>
        </div>
    )
}
