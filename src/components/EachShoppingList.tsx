import { getCountOfCertainItems } from '@/common/methods';
import { deleteItem, getAllShoppingList, updateStore } from '@/zustand/action';
import { Box, Card, IconButton, Typography } from '@mui/material'
import { useRouter } from 'next/navigation'
import React from 'react'
import { HiOutlineTrash } from 'react-icons/hi'

export default function EachShoppingList({data}:any) {
  const Router = useRouter();
  const allItemCount = data.items.length;
  const boughtItemsCount = getCountOfCertainItems(data.items,"bought",true)
  const openList = ()=>{
    updateStore({currentShoppingList:data});
    Router.push("/shoppingList")
  }
  return (
    <Box>
      {/* <button style={{width:"100%"}} className='mb-3'
       onClick={()=>{
          openList();
        }}
      > */}
        <Card className='px-3 py-1 flex-1 mb-3 rounded-xl bg-slate-100 pointer-events-auto hover:bg-sky-100 cursor-pointer'
          onClick={()=>{
            openList();
          }}
        >
        <div className='flex justify-between content-center pointer-events-auto'>
          <div className='content-center'>
            <Typography variant='h6' className='content-center'>
                 {data.title}
            </Typography>
            {/* <Typography variant='caption'>
                our dinner will be special for today
            </Typography> */}
          </div>
          <div className='justify-center items-center flex'>
          {/* <Checkbox
            checked={data.completed}
            color='success'

            onChange={(res)=>{
              
              updateReminderCompleted(data._id,res.target.checked)
            }}
            icon={<RadioButtonUncheckedIcon />}
            checkedIcon={<CheckCircleIcon />}
          /> */}
            <Typography variant='button' className='inline text-green-700 font-sans font-bold text-lg'>
                {boughtItemsCount}/{allItemCount}
            </Typography>

            <IconButton
              onClick={(e)=>{
                e.stopPropagation();
                deleteItem(data._id,"shopping-list","Shopping List", getAllShoppingList)
              }}
            >
                <HiOutlineTrash className='text-red-500' size={24}/>
            </IconButton>
            {/* <IconButton 
                onClick={()=>{
                    updateState({currentReminder:data})
                    action1();
                }}
            >
                <HiPencilAlt size={24} className='text-cyan-600'/>
            </IconButton> */}
          </div>
        </div>
      </Card>
      {/* </button> */}
    </Box>
  )
}
