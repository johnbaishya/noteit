"use client";
import ENV from '@/env';
import axios from 'axios';
import { useRouter } from 'next/navigation';
import React from 'react'
import {HiOutlineTrash} from 'react-icons/hi'

export default function RemoveBtn({id,click:onClick}:{id:String,click:Function}) {
  const router = useRouter();
  const removeTopic = async()=>{
    const confirmend = confirm("Are you sure");
    if(confirmend){
      axios.delete(ENV.API_URI+"topics?id="+id).then(res=>{
        router.refresh();
      })
    }
  }
  return (
    <button className='text-red-400'
      // onClick={click}
    >
        <HiOutlineTrash
            size={24}

        />
    </button>
  )
}
