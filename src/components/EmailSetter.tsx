"use client";
import ENV from '@/env';
import { Box, Button, Card, TextField, Typography } from '@mui/material'
import axios from 'axios';
import { useRouter } from 'next/navigation';
import React, { useState } from 'react'
import Cookies from 'universal-cookie';
import useAppStore  from "../zustand/store";

export default function EmailSetter() {
const [email,setEmail] = useState("");
const cookies = new Cookies(null, { path: '/' });
const router = useRouter();
const updateState = useAppStore(state=>state.updateState)

const saveEmail = ()=>{
    updateState({appInitializing:true})
    axios.post(ENV.API_URI+"users",{email})
    .then(res=>{
        cookies.set('user',res.data)
        updateState({user:res.data,appInitializing:false});
        router.push("/");
    })
}
  return (
    <>
    
            <Card className="p-10 mt-40 flex-col space-y-5 bg-slate-100">
                    <Typography variant='h4' className='text-center text-gray-500' gutterBottom >
                        Please provide your email address so that we can create a reference to save all your notes and tasks.
                    </Typography>
                    <Box className = "flex justify-center space-x-5" >
                    <TextField id="outlined-basic" label="email" variant="outlined" className='flex-1 bg-white'
                        onChange={(e)=>{
                            setEmail(e.target.value)
                        }}
                        value={email}
                    />
                    <Button onClick={saveEmail} variant='contained' color='success'>Continue</Button>
                </Box>
            </Card>
    </>
  )
}
