"use client"
import useAppStore from '@/zustand/store'
import { Backdrop, CircularProgress } from '@mui/material'
import React, { useState } from 'react'

export default function BackdropLoader() {

  const {appLoader} = useAppStore();
  return (
    <Backdrop open={appLoader} style={{zIndex:6}}>
        <CircularProgress color="primary" size={50} />
    </Backdrop>
  )
}
