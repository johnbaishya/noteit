import { deleteItem, getShoppingItems, saveShoppingItem } from '@/zustand/action'
import { Box, Card, Checkbox, IconButton, Typography } from '@mui/material'
import React from 'react'
import { HiOutlineTrash } from 'react-icons/hi'

export default function EachShoppingItem({data,sn}:any) {
  return (
    <Box className="flex content-center align-middle mb-2 ">
      <div className='align-middle content-center w-8'>
        <Typography className='align-middle justify-center'>
          {sn+1}
        </Typography>
      </div>

        <Card className='p-0 flex flex-1 content-center bg-zinc-100 rounded-md'>
          <div className='flex-1 flex'>
            <Checkbox size='large' color='success'
              checked={data.bought}
              onChange={(e)=>{
                saveShoppingItem({bought:e.target.checked},data._id)
              }}
            />
            <Typography variant='body1' className='font-sans font-semibold content-center'>
                {data.title}
            </Typography>
          </div>
          <IconButton
            onClick={()=>{deleteItem(data._id,"shopping-item","Item", getShoppingItems)}}
          >
            <HiOutlineTrash
                className='text-red-500'
                size={24}

            />
          </IconButton>
        </Card>
    </Box>
  )
}
