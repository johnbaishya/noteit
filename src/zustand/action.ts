import axios from "axios";
import useAppStore from "./store";
import ENV from "@/env";









export const updateStore = (data:any)=>{
    useAppStore.getState().updateState(data);
}








export const getNoteList = ()=>{
    const {notes,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        if(notes.length==0){
            updateStore({notesLoading:true})
        }
        axios.get(ENV.API_URI+"notes?user_id="+user._id)
        .then(res=>{
            updateStore({
                notesLoading:false,
                notes:res.data
            })
            resolve(res);
        })
        .catch(err=>{
            updateStore({
                notesLoading:false,
                error:true,
                errorMessage:"error while getting Notes"
            });
            reject(err);
        })
    })
}








export const getReminderList = ()=>{
    const {user,reminders} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        if(reminders.length==0){
            updateStore({remindersLoading:true})
        }
        axios.get(ENV.API_URI+"reminder?user_id="+user._id).then(res=>{
            updateStore({
                reminders:res.data,
                remindersLoading:false,
            })
            resolve(res);
        }).catch(err=>{
            updateStore({
                remindersLoading:false,
                error:true,
                errorMessage:"error while getting Reminders"
            })
            reject(err);
        })

    })
}

export const withUser = (data:any)=>{
    const {user} = useAppStore.getState();
    return{
        ...data,
        user_id:user._id
    }
}   


export const updateReminderToServer = (id:string,data:any)=>{
    return new Promise((resolve, reject) => {
        const dataWithuser = withUser(data)
        axios.post(ENV.API_URI+"/reminder"+id,dataWithuser)
        .then(res=>{
            updateStore({
                appLoader:false,
                currentReminder:{},
                success:true,
                successMessage:"Reminder Saved Successfully"
            });
            getReminderList();
            resolve(res);
        }).catch(err=>{
            updateStore({
                appLoader:false,
                error:true,
                errorMessage:"Error while Adding Reminder"
            })
            reject(err);
            console.log("error while saving reminder",err)
        })
    })
}




export const saveReminder = ()=>{
    const {currentReminder,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        updateStore({
            appLoader:true,
        })    
        const data = {
            ...currentReminder,
            // completed:false,
            user_id:user._id,
        }
        let id  = currentReminder?._id?"?id="+currentReminder._id:"";
        updateReminderToServer(id,data).then(res=>{
            resolve(res)
        }).catch(err=>{
            reject(err)
        })
        
    })
}


export const updateReminderCompleted = (id:string,status:boolean)=>{
    let newID  = id?"?id="+id:"";
    updateStore({
        appLoader:true,
    })
    return new Promise((resolve, reject) => {
        updateReminderToServer(newID,{completed:status})
        .then(res=>{
            resolve(res)
            updateStore({
                successMessage:"status changed successfully"
            })
        })
        .catch(err=>{
            reject(err);
            updateStore({
                successMessage:"error while changing status"
            })
        })
    })
}










export const saveNote = ()=>{
    const {currentNote,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        updateStore({
            appLoader:true,
        })
        const {title,text} = currentNote;
        let id  = currentNote?._id?"?id="+currentNote._id:"";
        axios.post(ENV.API_URI+"notes"+id,{title,text,user_id:user._id})
        .then(res=>{
            updateStore({
                appLoader:false,
                success:true,
                successMessage:"Note Saved Successfully"
            })        
            resolve(res)
        })
        .catch(err=>{
            console.log("err",err);
            updateStore({
                appLoader:false,
                error:true,
                errorMessage:"Error While Adding Note"
            })
            reject(err)
        })
    })
}


export const deleteItem = (id:string,key:string,name:string, callback:Function)=>{
    return new Promise((resolve, reject) => {
            const confirmend = confirm("Are you sure");
            if(confirmend){
                updateStore({appLoader:true})
                axios.delete(ENV.API_URI+key+"/"+id)
                .then(res=>{
                    updateStore({appLoader:false,success:true,successMessage:name+" Deleted Success"});
                    callback();
                    resolve(res);
                }).catch(err=>{
                    console.log(err);
                    updateStore({appLoader:false,error:false,errorMessage:"Error while deleting "+name});
                    reject(err)
                })
            }
    })
    
}


export const saveShoppingList = ()=>{
    const {currentShoppingList,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        updateStore({
            appLoader:true,
        })
        const {title} = currentShoppingList;
        let id  = currentShoppingList?._id?"?id="+currentShoppingList._id:"";
        let pData = {title,user_id:user._id}
        axios.post(ENV.API_URI+"shopping-list"+id,pData)
        .then(res=>{
            updateStore({
                appLoader:false,
                success:true,
                successMessage:"Shopping List Saved Successfully",
                currentShoppingList:res.data,
            })        
            resolve(res)
        })
        .catch(err=>{
            console.log("err",err);
            updateStore({
                appLoader:false,
                error:true,
                errorMessage:"Error While Saving Shopping List"
            })
            reject(err)
        })
    })
}


export const getAllShoppingList = ()=>{
    const {allShoppingList,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        if(allShoppingList.length==0){
            updateStore({shoppingListLoading:true})
        }
        axios.get(ENV.API_URI+"shopping-list?user_id="+user._id)
        .then(res=>{
            updateStore({
                shoppingListLoading:false,
                appLoader:false,
                allShoppingList:res.data,
            })
        }).catch((err:any)=>{
            console.log("error",err)
            updateStore({
                shoppingListLoading:false,
                appLoader:false,
                error:true,
                errorMessage:"Something Went Wrong while loading shopping lists"
            })
        })
    })
}


export const getShoppingItems = ()=>{
    const {currentShoppingList,shoppingItems} = useAppStore.getState();
        if(shoppingItems.length==0){
            updateStore({shoppingItemsLoading:true})
        }
    axios.get(ENV.API_URI+"shopping-item?shopping_list_id="+currentShoppingList._id)
    .then(res=>{
        updateStore({
            shoppingItems:res.data,
            shoppingItemsLoading:false,
        })
    })
    .catch(err=>{
        updateStore({
            shoppingItemsLoading:false,
            error:true,
            errorMessage:"Error While loading Items"
        })
    })
}




export const saveShoppingItem = (newData:any,id:any)=>{
    const {currentShoppingList,currentShoppingItem} = useAppStore.getState();
    updateStore({
        appLoader:true,
    })
    return new Promise((resolve, reject) => {
        let pData = {
            // title:currentShoppingItem.title,
            shoppingList:currentShoppingList._id,
            bought:false,
            ...newData
        }

        let newId =   id?"?id="+id:"";
        axios.post(ENV.API_URI+"/shopping-item"+newId,pData)
        .then(res=>{
            getShoppingItems();
            resolve(res);
            updateStore({
                appLoader:false,
                success:true,
                successMessage:"Item Saved Successfully"
            })
        })
        .catch(err=>{
            reject(err)
            updateStore({
                appLoader:false,
                error:true,
                errorMessage:"Error while Saving Item"
            })
        })
    })
}


// =============================================================================
// for schedule and task

export const saveSchedule = ()=>{
    const {currentSchedule,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        updateStore({
            appLoader:true,
        })
        const {title} = currentSchedule;
        let id  = currentSchedule?._id?"?id="+currentSchedule._id:"";
        let pData = {title,user:user._id}
        axios.post(ENV.API_URI+"schedule"+id,pData)
        .then(res=>{
            updateStore({
                appLoader:false,
                success:true,
                successMessage:"Schedule Saved Successfully",
                currentSchedule:res.data,
            })        
            resolve(res)
        })
        .catch(err=>{
            console.log("err",err);
            updateStore({
                appLoader:false,
                error:true,
                errorMessage:"Error While Saving Schedule"
            })
            reject(err)
        })
    })
}


export const getSchedules = ()=>{
    const {schedules,user} = useAppStore.getState();
    return new Promise((resolve, reject) => {
        if(schedules.length==0){
            updateStore({schedulesLoading:true})
        }
        axios.get(ENV.API_URI+"schedule?user_id="+user._id)
        .then(res=>{
            updateStore({
                schedulesLoading:false,
                appLoader:false,
                schedules:res.data,
            })
        }).catch((err:any)=>{
            console.log("error",err)
            updateStore({
                schedulesLoading:false,
                appLoader:false,
                error:true,
                errorMessage:"Something Went Wrong while loading Schedules"
            })
        })
    })
}


export const getTasks= ()=>{
    const {currentSchedule,tasks} = useAppStore.getState();
        if(tasks.length==0){
            updateStore({tasksLoading:true})
        }
    axios.get(ENV.API_URI+"task?schedule_id="+currentSchedule._id)
    .then(res=>{
        updateStore({
            tasks:res.data,
            tasksLoading:false,
        })
    })
    .catch(err=>{
        updateStore({
            tasksLoading:false,
            error:true,
            errorMessage:"Error While loading Tasks"
        })
    })
}




export const saveTask= (newData:any,id:any)=>{
    const {currentSchedule,currentShoppingItem} = useAppStore.getState();
    updateStore({
        appLoader:true,
    })
    return new Promise((resolve, reject) => {
        let pData = {
            // title:currentShoppingItem.title,
            schedule:currentSchedule._id,
            completed:false,
            ...newData
        }
        console.log("pdata",pData)

        let newId =   id?"?id="+id:"";
        axios.post(ENV.API_URI+"/task"+newId,pData)
        .then(res=>{
            getTasks();
            resolve(res);
            updateStore({
                appLoader:false,
                success:true,
                successMessage:"Task Saved Successfully"
            })
        })
        .catch(err=>{
            reject(err)
            updateStore({
                appLoader:false,
                error:true,
                errorMessage:"Error while Saving Task"
            })
        })
    })
}

