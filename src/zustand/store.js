import {create} from "zustand";

const initialState = {
    user:{},
    notes:[],
    notesLoading:false,
    currentNote:{},
    appLoader:false,
    reminders:[],
    remindersLoading:false,
    currentReminder:{},
    allShoppingList:[],
    shoppingListLoading:false,
    currentShoppingList:{},
    shoppingItems:[],
    shoppingItemsLoading:false,
    currentShoppingItem:{},
    schedules:[],
    schedulesLoading:false,
    currentSchedule:{},
    tasks:[],
    tasksLoading:false,
    currentTask:{},
    error:false,
    errorMessage:"",
    success:false,
    successMessage:"",
    mainTabValue:"1",
    appInitializing:true,
    
}


const useAppStore = create((set)=>{
    return {
        ...initialState,
        updateState:(data)=>set(state=>({
            ...state,
            ...data
        })),
        reset:()=>set(initialState)
    }
});

export default useAppStore;