import mongoose, { Schema, mongo } from "mongoose";


// model for user ====================================================================
const userSchema = new Schema(
    {
        email:String,
    },
    {
        timestamps:true,
    }
)
export const User = mongoose.models.User||mongoose.model("User",userSchema);






// model for Note ===================================================================
const noteSchema = new Schema(
    {
        title:String,
        text:String,
        email:String, 
        user:{type:mongoose.Types.ObjectId,required:true,ref:"User"}
    },
    {
        timestamps:true,
    }
)
export const Note = mongoose.models.Note||mongoose.model("Note",noteSchema);




// model for shopping category ==================================================================
const shoppingCategorySchema = new Schema(
    {
        name:String,
        description:String,
    },
    {
        timestamps:true,
    }
)
export const ShoppingCategory = mongoose.models.ShoppingCategory||mongoose.model("ShoppingCategory",shoppingCategorySchema);





// model for Shopping list ===============================================================================
const shoppingListSchema  = new Schema(
    {
        title:String,
        description:String,
        category:{type:mongoose.Types.ObjectId,ref:"ShoppingCategory"},
        user:{type:mongoose.Types.ObjectId,ref:"User"}
    },
    {
        timestamps:true,
    }
)

shoppingListSchema.virtual("items",{
    ref:"ShoppingItem",
    foreignField:"shoppingList",
    localField:"_id",
    justOne:false,

});

shoppingListSchema.set('toJSON',{virtuals:true});

export const ShoppingList = mongoose.models.ShoppingList||mongoose.model("ShoppingList",shoppingListSchema);






// model for Shopping Item ================================================================================
const shoppingItemSchema  = new Schema(
    {
        title:String,
        description:String,
        expected_price:Number,
        actual_price:Number,
        bought:Boolean,
        boughtDate:Date,
        category:{type:mongoose.Types.ObjectId,ref:"ShoppingCategory"},
        shoppingList:{type:mongoose.Types.ObjectId,ref:"ShoppingList"}
    },
    {
        timestamps:true,
    }
)
export const ShoppingItem = mongoose.models.ShoppingItem||mongoose.model("ShoppingItem",shoppingItemSchema);





// model for Schedule =========================================================================================
const scheduleSchema  = new Schema(
    {
        title:String,
        description:String,
        date:Date,
        user:{type:mongoose.Types.ObjectId,required:true,ref:"User"}
    },
    {
        timestamps:true,
    }
)
export const Schedule = mongoose.models.Schedule||mongoose.model("Schedule",scheduleSchema);





// model for tasks ===========================================================================================
const taskSchema  = new Schema(
    {
        title:String,
        description:String,
        completed:Boolean,
        date:Date,
        schedule:{type:mongoose.Types.ObjectId,required:true,ref:"Schedule"}
    },
    {
        timestamps:true,
    }
);
export const Task  = mongoose.models.Task||mongoose.model("Task",taskSchema);






// model for reminder  ===================================================================================
const reminderSchema = new Schema(
    {
        title:String,
        description:String,
        appointment_date:Date,
        completed:Boolean,
        time:String,
        user:{type:mongoose.Types.ObjectId,required:true,ref:"User"},
    },
    {
        timestamps:true,
    }
)
export const  Reminder = mongoose.models.Reminder||mongoose.model("Reminder",reminderSchema);